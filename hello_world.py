print("Hello World")

planete = {
    'Earth': 1.0,
    'Mercury': 0.378,
    'the Moon': 0.166,
    'Jupiter': 2.364
}

masse = 42

for key, value in planete.items():
    print("The mass of the object on {name} is {mass}.".format(name=key,mass=value*masse))
